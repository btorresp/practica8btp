package com.example.practica7btp

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.LinearLayout
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.practica7btp.helper.SwipeToDeleteCallback
import com.example.practica7btp.model.Contact
import com.example.practica7btp.ui.ContactDialogFragment
import com.example.practica7btp.ui.NoticeDialogListener
import com.example.practica7btp.widget.ContactsAdapter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*




class MainActivity : AppCompatActivity(), NoticeDialogListener {

    private var Contacts = mutableListOf(
        Contact("Brenda", 32, "brendys.torres87@gmail.com"),
        Contact("Yareli", 10, "lili@correo.com"),
        Contact("Jaziel", 7, "jaziel@correo")
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        list_recycler_view.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = ContactsAdapter(Contacts)
            addItemDecoration(DividerItemDecoration(context,LinearLayout.VERTICAL))
        }

        fab.setOnClickListener { view ->

            val newFragment = ContactDialogFragment()
            newFragment.show(supportFragmentManager, "contacto")

            //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                  //  .setAction("Action", null).show()
        }

    //Clase para eliminar contacto

        val swipeHandler = object: SwipeToDeleteCallback(this){
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val adapter = list_recycler_view.adapter as ContactsAdapter
                adapter.removeAt(viewHolder.adapterPosition)
            }
        }
        val itemTouchHelper = ItemTouchHelper(swipeHandler)
        itemTouchHelper.attachToRecyclerView(list_recycler_view)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onDialogPositiveClick(dialog: DialogFragment, contacto: Contact) {
        val adapter = list_recycler_view.adapter as ContactsAdapter
        adapter.add(contacto)

        Snackbar.make(findViewById(android.R.id.content), "${contacto.name}Agregado", Snackbar.LENGTH_LONG)
          .setAction("Action", null).show()
    }
    override fun onDialogNegativeClick(dialog: DialogFragment) {
        Snackbar.make(findViewById(android.R.id.content), "Cancelado", Snackbar.LENGTH_LONG)
            .setAction("Action", null).show()
    }

}
