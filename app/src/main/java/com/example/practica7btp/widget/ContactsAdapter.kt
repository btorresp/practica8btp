package com.example.practica7btp.widget

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.practica7btp.model.Contact

class ContactsAdapter(private var list: MutableList<Contact>) : RecyclerView.Adapter<ContactViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactViewHolder {
       val inflater =LayoutInflater.from(parent.context)
        return ContactViewHolder(inflater, parent)

    }

    override fun getItemCount(): Int {
    return list.size
    }

    override fun onBindViewHolder(holder: ContactViewHolder, position: Int) {
        val contact: Contact= list[position]
        holder.bind(contact)
    }
    fun add(contacto: Contact){
        list.add(contacto)
        notifyItemInserted(list.size - 1)
    }

    fun removeAt(position: Int){
        list.removeAt(position)
        notifyItemRemoved(position)
    }
}



