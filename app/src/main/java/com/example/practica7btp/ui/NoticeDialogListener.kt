package com.example.practica7btp.ui

import androidx.fragment.app.DialogFragment
import com.example.practica7btp.model.Contact


interface NoticeDialogListener {
    fun onDialogPositiveClick(dialog: DialogFragment, contacto: Contact)
    fun onDialogNegativeClick(dialog: DialogFragment)

}