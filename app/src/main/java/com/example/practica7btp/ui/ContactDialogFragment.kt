package com.example.practica7btp.ui

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import com.example.practica7btp.R
import com.example.practica7btp.model.Contact
import kotlinx.android.synthetic.main.dialog_contact.*
import kotlinx.android.synthetic.main.dialog_contact.view.*
import kotlin.ClassCastException

class ContactDialogFragment: DialogFragment() {

    internal lateinit var listener: NoticeDialogListener

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let{
            val builder = AlertDialog.Builder(it)
            val inflater = requireActivity().layoutInflater
            val view = inflater.inflate(R.layout.dialog_contact, null)
            builder.setTitle("Agregar contacto")
            builder.setView(view)
                .setPositiveButton("Ok",
                    DialogInterface.OnClickListener{ dialogInterface, i ->

                    var contacto = Contact(
                        view.txtName.editableText.toString(),
                        view.txtAge.editableText.toString().toInt(),
                        view.txtEmail.editableText.toString()
                    )
                     /*
                        if (view.txtName.equals(""))
                            view.txtName.setError("campo vacio")
                        if(view.txtAge.equals(""))
                            view.txtAge.setError("campo vacio")
                        if(view.txtEmail.equals(""))
                            view.txtEmail.setError("campo vacio")

                            Intente poner las validaciones pero me da error en el campo de Edad
                     */
                      listener.onDialogPositiveClick(this,  contacto)
                })

                .setNegativeButton("Cancelar", DialogInterface.OnClickListener{
                    dialogInterface, i ->
                    listener.onDialogNegativeClick(this)
                })
            builder.create()
        } ?: throw IllegalStateException("la actividad no puede ser vacia")
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        try {
            listener = context as NoticeDialogListener
        }catch (e : ClassCastException){
            throw ClassCastException("Debe implementar la interfaz correctamente")
        }
    }
}